package com.example.demo.controller;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Comment;
import com.example.demo.entity.Report;
import com.example.demo.service.CommentService;
import com.example.demo.service.ReportService;

//controllerに指定するservletには必ず以下のアノテーションを付与する
@Controller
public class ForumController {
	@Autowired
	ReportService reportService;
	@Autowired
	CommentService commentService;
	//@Autowiredで作成したインスタンスは使用のたびにnewする必要なくインスタンス生成する

	// 投稿内容表示画面。トップ画面にアクセスした場合に表示
	@GetMapping
	public ModelAndView top() {
		ModelAndView mav = new ModelAndView();
		// 投稿を全件取得
		List<Report> contentData = reportService.findAllReport();
		//コメントを取得
		List<Comment> commentData = commentService.findAllComment();
		// 画面遷移先を指定
		mav.setViewName("/top");
		// データオブジェクトを保管
		mav.addObject("contents", contentData);
		mav.addObject("comments", commentData);
		return mav;
	}

	@GetMapping("/narrow/*")
	public ModelAndView narrow(@RequestParam("startDate") String startDate, @RequestParam("endDate") String endDate) {
		ModelAndView mav = new ModelAndView();
		// 投稿を全件取得
		List<Report> contentData = reportService.findAllReport(startDate, endDate);
		//コメントを取得
		List<Comment> commentData = commentService.findAllComment();
		// 画面遷移先を指定
		mav.setViewName("/top");
		// データオブジェクトを保管
		mav.addObject("contents", contentData);
		mav.addObject("comments", commentData);
		return mav;
	}

	// 新規投稿画面。"/new"にアクセスした場合に表示
	@GetMapping("/new")
	public ModelAndView newContent() {
		// ModelAndViewは利用するデータとViewに関する情報を管理し、Timeleafを用いたテンプレート(html)へ引き渡す。JSPのrequest/responseのようなもの
		ModelAndView mav = new ModelAndView();
		// form用の空のentityを準備
		Report report = new Report();
		// 画面遷移先を指定
		mav.setViewName("/new");
		// 準備した空のentityを保管
		mav.addObject("formModel", report);
		return mav;
	}

	// 投稿処理。ここではpostによるデータ処理を行う。ボタンに"/add"をリンクさせる
	@PostMapping("/add")
	public ModelAndView addContent(@ModelAttribute("formModel") Report report) {
	//@ModelAttributeはメソッドを呼び出し、その戻り値をModel(ここではformModel)に詰める。
		// 投稿をテーブルに格納

		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		report.setCreatedDate(timestamp);
		report.setUpdatedDate(timestamp);
		reportService.saveReport(report);
		// rootへリダイレクト。rootはtopなのでtopメソッドが再起動する
		return new ModelAndView("redirect:/");
	}

	@GetMapping("/delete/{id}")
	public ModelAndView deleteContent(@PathVariable Integer id) {
		// 投稿を指定してdeletereortを実行
		reportService.deleteReport(id);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	// 投稿の編集(top画面からの遷移)
	@GetMapping("/edit/{id}")
	public ModelAndView editContent(@PathVariable Integer id) {
	//@PathVariableはURLの一部を引数として使用したいときに用いる。Getメソッドに乗るのが一般的
		ModelAndView mav = new ModelAndView();
		// 編集する投稿を取得
		Report report = reportService.editReport(id);
		// 編集する投稿をセット
		mav.addObject("formModel", report);
		// 画面遷移先を指定
		mav.setViewName("/edit");
		return mav;
	}

	// 編集処理(編集画面からのput(サーバの更新・追加。postとの使い分けに注意)メソッドと紐付け)
	@PutMapping("/update/{id}")
	public ModelAndView updateContent(@PathVariable Integer id, @ModelAttribute("formModel") Report report) {
		// UrlParameterのidを更新するentityにセット
		report.setId(id);
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		report.setUpdatedDate(timestamp);
		// 編集した投稿を更新
		reportService.saveReport(report);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	// コメント投稿画面(top画面からの遷移)
	@GetMapping("/comment/{id}")
	public ModelAndView newComment(@PathVariable Integer id) {
		// ModelAndViewは利用するデータとViewに関する情報を管理し、Timeleafを用いたテンプレート(html)へ引き渡す。JSPのrequest/responseのようなもの
		ModelAndView mav = new ModelAndView();
		// CommentIdをformModelに引き渡し
		mav.addObject("commentId", id);
		// 画面遷移先を指定
		mav.setViewName("/comment");
		return mav;
	}

	@PostMapping("/addComment")
	public ModelAndView addComment(@ModelAttribute("commentId") Integer commentId, @ModelAttribute("content") String content) {
		// 編集した投稿を更新
		Comment comment = new Comment();
		comment.setCommentId(commentId);
		comment.setContent(content);
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		comment.setCreatedDate(timestamp);
		comment.setUpdatedDate(timestamp);
		commentService.saveComment(comment);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	@GetMapping("/deleteComment/{id}")
	public ModelAndView deleteComment(@PathVariable Integer id) {
		// 投稿を指定してdeletereortを実行
		commentService.deleteComment(id);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	@GetMapping("/editComment/{id}")
	public ModelAndView editComment(@PathVariable Integer id) {
	//@PathVariableはURLの一部を引数として使用したいときに用いる。Getメソッドに乗るのが一般的
		ModelAndView mav = new ModelAndView();
		// 編集する投稿を取得
		Comment comment = commentService.editComment(id);
		// 編集する投稿をセット
		mav.addObject("formModel", comment);
		// 画面遷移先を指定
		mav.setViewName("/editComment");
		return mav;
	}

	@PutMapping("/updateComment/{id}")
	public ModelAndView updateComment(@PathVariable Integer id, @ModelAttribute("formModel") Comment comment) {
		// UrlParameterのidを更新するentityにセット
		comment.setId(id);
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		comment.setUpdatedDate(timestamp);
		// 編集した投稿を更新
		commentService.saveComment(comment);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}
}
