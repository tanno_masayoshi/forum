package com.example.demo.repository;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Report;

@Repository
public interface ReportRepository extends JpaRepository<Report, Integer> {
/*JpaRepositoryにはいくつかのメソッドが既存として組み込まれており、
この受け子としてReportRepositoryはインターフェースを形成することから、何も記述がなくても作動する。
一方で、以下のように明示的に記述することでこの型に合わせたSQL文を自動生成することもできる。
*/
	List<Report> findByCreatedDateBetweenOrderByCreatedDateDesc(Timestamp startTimestamp, Timestamp endTimestamp);
}
