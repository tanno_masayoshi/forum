package com.example.demo.service;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Report;
import com.example.demo.repository.ReportRepository;

//serviceに指定するservletには必ず以下のアノテーションを付与する
@Service
public class ReportService {
	/*ServiceにはDao(entity、Dtoへのデータの受け渡しを定義するxmlの読み込み)処理を
	実施するためのビジネスロジックを記載する*/
	@Autowired
	ReportRepository reportRepository; //JpaRepositoryを用いて投稿内容を管理する

	// レコード全件取得
	public List<Report> findAllReport() {
		return reportRepository.findAll(Sort.by(Sort.Direction.DESC, "createdDate"));
		//findAllOrderByCreatedDateDeskと同じ。上記はsortメソッドを用いたもので、こちらはSQL文として動作させるもの
	}

	public List<Report> findAllReport(String startDate, String endDate) {
		SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date today = new Date();
		Timestamp startTimestamp = null;
		Timestamp endTimestamp = null;
		System.out.println(startDate);
		if (startDate == null || startDate == "") {
			startTimestamp = Timestamp.valueOf("2020-01-01 00:00:00");
		} else {
			startTimestamp = Timestamp.valueOf(startDate + " 00:00:00");
		}
		if (endDate == null || endDate == "") {
			endTimestamp = Timestamp.valueOf(sdFormat.format(today).toString() + " 23:59:59");
		} else {
			endTimestamp = Timestamp.valueOf(endDate + " 23:59:59");
		}
		return reportRepository.findByCreatedDateBetweenOrderByCreatedDateDesc(startTimestamp, endTimestamp);
	}

	//レコードの追加
	public void saveReport(Report report) {
		reportRepository.save(report); //JpaRepositoryのSaveメソッドはテーブルへのinsert、updateのような処理を持っている
	}

	//レコードの取得
	public Report editReport(Integer id) {
		Report report = (Report) reportRepository.findById(id).orElse(null);
		return report;
	}

	//レコードの削除
	public void deleteReport(Integer id) {
		reportRepository.deleteById(id);
	}
}
