package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Comment;
import com.example.demo.repository.CommentRepository;

//serviceに指定するservletには必ず以下のアノテーションを付与する
@Service
public class CommentService {
	/*ServiceにはDao(entity、Dtoへのデータの受け渡しを定義するxmlの読み込み)処理を
	実施するためのビジネスロジックを記載する*/
	@Autowired
	CommentRepository commentRepository; //JpaRepositoryを用いて投稿内容を管理する

	// コメント全件取得
	public List<Comment> findAllComment() {
		return commentRepository.findAll(Sort.by(Sort.Direction.DESC, "createdDate"));
	}

	//コメントの追加
	public void saveComment(Comment comment) {
		commentRepository.save(comment); //JpaRepositoryのSaveメソッドはテーブルへのinsert、updateのような処理を持っている
	}

	public void deleteComment(Integer id) {
		commentRepository.deleteById(id);
	}

	public Comment editComment(Integer id) {
		Comment comment = (Comment) commentRepository.findById(id).orElse(null);
		return comment;
	}
}
